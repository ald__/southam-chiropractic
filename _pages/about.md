---
title: About Us
layout: default
permalink: /about/
banner:
    image: 678898036
    text: About Us
three_images:
    - 244733887
    - 636736618
    - 1335251387
---

# {{page.title}}
## About chiropractic
Chiropractic is the third largest primary health care profession in the world after Medicine and Dentistry, and is concerned with the diagnosis, treatment and prevention of mechanical disorders of the musculoskeletal system (bones, joints and muscles), and the effects of these disorders on the function of the nervous system and general health. We have a specialist interest in neck and back pain but take the entire physical, emotional and social wellbeing into account with every patient.

{% include three_images %}

## Common conditions we encounter
Below is a list of conditions we regularly see in practice as having a significant, detrimental impact on our client's lives:
- Joint Pain
- Headaches and Migraines
- Muscle Pain/Spasm
- Sciatica
- Back Pain
- Neck Pain
- Shoulder Pain
- Pelvic Pain

## Pregnant women, babies and children
Our Chiropractors are also trained to work with pregnant women, babies and children. We can offer safe, gentle care for relief of discomfort during pregnancy as well as treatment suitable for children and babies.