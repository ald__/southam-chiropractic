const { src, dest, series, parallel, watch, lastRun } = require('gulp');
const autocrop = require('gulp-autocrop');
const rename = require('gulp-rename');

const imagemin = 		require('gulp-imagemin');
const webp = 			require('imagemin-webp');
const extReplace = 		require('gulp-ext-replace');

function reduce (cb) {
    src('assets/images/raw_stock/*.jpg')
        .pipe(autocrop({
            width: 1000
        }))
        .pipe(dest('assets/images/raw_stock/'))
        .on('finish', cb);
};


function iconTask (cb) {
	const sizes = [32, 64, 68, 76, 120, 128, 152, 256, 512];
	let count = 0;
	sizes.forEach(function (size) {
		src('assets/images/icon.png', { since: lastRun(iconTask, 1000) })
			.pipe(autocrop({
				height: size,
				width: size
			}))
			.pipe(rename({
				basename: size
			}))
			.pipe(imagemin())
			.pipe(dest('assets/icons'))
			.on('finish', () => {
				count+=1;
				if(count === sizes.length)
					cb();
			})
	})
}

function ratio_96_55 (cb) {
    const sizes = [960,720,640,580,320];
    let count = 0;
    sizes.forEach((size) => {
        src('assets/images/raw_stock/*.jpg', { since: lastRun(ratio_96_55, 1000) })
            .pipe(autocrop({
                width: size,
                ratio: '96:55' //960x550
            }))
            .pipe(imagemin())
            .pipe(dest(`assets/images/stock/96_55/${size}/`))
            .pipe(imagemin([
                webp({
                    quality: 90
                })
            ]))
            .pipe(extReplace('.webp'))
            .pipe(dest(`assets/images/stock/96_55/${size}/`))
            .on('finish', () => {
                count++;
                if(count === sizes.length)
                    cb();
            });
    });
};

function ratio_42_29 (cb) {
    const sizes = [720,640,580,320];
    let count = 0;
    sizes.forEach((size) => {
        src('assets/images/raw_stock/*.jpg', { since: lastRun(ratio_42_29, 1000) })
            .pipe(autocrop({
                width: size,
                ratio: '42:29' //420x290
            }))
            .pipe(imagemin())
            .pipe(dest(`assets/images/stock/42_29/${size}/`))
            .pipe(imagemin([
                webp({
                    quality: 90
                })
            ]))
            .pipe(extReplace('.webp'))
            .pipe(dest(`assets/images/stock/42_29/${size}/`))
            .on('finish', () => {
                count++;
                if(count === sizes.length)
                    cb();
            });
    });
};

exports.reduce = reduce;
exports.icons = iconTask;
exports.default = series(iconTask,ratio_96_55,ratio_42_29);